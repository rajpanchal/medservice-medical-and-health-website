# MedService Medical & Health
A website for fictional MedService Medical & Health developed and deployed with following proper process.
The BEM process is followed as naming conventions.

## Build With
1. HTML
2. CSS
3. JavaScript (Object Oritented Programming)
4. jQuery v3.5.1

## Tools Used
1. Git
2. Node
3. Webpack
4. PostCSS 

## Development Dependencies
1. autoprefixer v9.7.6
2. clean-webpack-plugin v3.0.0
3. css-loader v3.5.2
4. cssnano v4.1.10
5. fs-extra v9.0.0
6. html-webpack-plugin v4.3.0
7. mini-css-extract-plugin v0.9.0
8. postcss-import v12.0.1
9. postcss-loader v3.0.0
10. postcss-mixins v6.2.3
11. postcss-nested v4.2.1
12. postcss-simple-vars v5.0.2
13. style-loader v1.1.4
14. webpack v4.42.1
15. webpack-cli v3.3.11
16. webpack-dev-server v3.10.3

## Plugins Used
1. [normalize.css v8.0.1](https://necolas.github.io/normalize.css/)
2. [jquery-smooth-scroll v2.2.0](https://plugins.jquery.com/smooth-scroll/)
3. [lazysizes v5.2.2](http://afarkas.github.io/lazysizes/lazysizes.min.js)
4. [waypoints v4.0.1](http://imakewebthings.com/waypoints/)

## License
MIT © [Raj Panchal](https://gitlab.com/rajpanchal)